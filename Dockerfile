# stage1 as builder
FROM node:latest as builder

WORKDIR /backend

# Copy the package.json and install dependencies
COPY package*.json ./
RUN npm install
# RUN npm install --save cors
COPY . .

# Build the project
# RUN npm i -g pm2 
# RUN pm2 start index.js

# EXPOSE 3001
ENTRYPOINT ["node", "index.js"]
